//D2, the chosen digital input pin
const int button = 2;

//The Arduino Setup function that is ran first to
//initialize the program
void setup() {
  Serial.begin(115200);  //initial the Serial
  pinMode(button, INPUT); //Sets the button to an input
  }


//The Arduino Loop that runs forever
void loop() {

  //Check if the button is being pressed
  //the button is active high
if(digitalRead(button) == HIGH){
        help();
    }
  
  //Checks if serial is available, meaning if something is in the buffer
  if (Serial.available())  {

    //checks if the button is pressed
    if(digitalRead(button) == HIGH){
        help();
    }

    //Reads whatever is in the buffer, used for running our test
    char c = Serial.read();  //gets one byte from serial buffer
    if (c == 't') {
      test();
    }

    
  }
}

//Test, if the letter 't' is recieved, this sends a message back to confirm confirmation
void test(){
  Serial.write("Test Recived v2!");
  Serial.println();
}

//The message that will be sent when the button is pressed
void help(){
  Serial.write("Help");
  Serial.println();
  delay(200); //The delay is used as without it, it spammed the console very rapidly
}